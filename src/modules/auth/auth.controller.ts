import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  UseGuards,
  Request,
  UseInterceptors,
  Logger,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { Public } from './auth.decorator';
import { LoggingInterceptor } from './logging.interceptor';
import { ApiResponse } from '../common/classes/api-result';

@Controller('auth')
export class AuthController {
  private readonly logger = new Logger(AuthController.name);

  constructor(private authService: AuthService) {}

  @Public()
  @HttpCode(HttpStatus.OK)
  @Post('login')
  signIn(@Body() signInDto: Record<string, any>) {
    return this.authService.signIn(signInDto.username, signInDto.password);
  }

  @UseInterceptors(LoggingInterceptor)
  @Get('profile')
  getProfile(@Request() req) {
    this.logger.log('Start...');
    return new ApiResponse().success(req.user, 'ok');
  }
}
