import { HttpStatus } from '@nestjs/common';

export class ApiResponse<T> {
  constructor(
    public data?: T,
    public statusCode?: HttpStatus,
    public message?: string,
  ) {}

  public success(data?: T, message?: string): ApiResponse<T> {
    return new ApiResponse<T>(data, HttpStatus.OK, message);
  }

  public error(message: string, statusCode: HttpStatus): ApiResponse<T> {
    return new ApiResponse<T>(null, statusCode, message);
  }
}
