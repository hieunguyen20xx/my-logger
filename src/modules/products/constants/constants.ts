export enum ProductType {
  CLOTHES = 'CLOTHES',
  PANTS = 'PANTS',
  SHOES = 'SHOES',
}
