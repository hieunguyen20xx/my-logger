import { Injectable } from '@nestjs/common';
import { ProductType } from '../constants/constants';
import { ProductInterface } from '../interfaces/product.interface';
import { Clothes } from '../objects/clothes';
import { Pants } from '../objects/pants';
import { Shoes } from '../objects/shoes';

@Injectable()
export class ProductFactory {
  constructor(
    private clothes: Clothes,
    private pants: Pants,
    private shoes: Shoes,
  ) {}

  async createObject(objectType: ProductType): Promise<ProductInterface> {
    switch (objectType) {
      case ProductType.CLOTHES:
        return this.clothes;
      case ProductType.PANTS:
        return this.pants;
      case ProductType.SHOES:
        return this.shoes;
    }
  }
}
