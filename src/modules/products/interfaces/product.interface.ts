export interface ProductInterface {
  getInfo(id: number): string;
  getData(): any;
}
