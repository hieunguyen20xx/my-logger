import { ProductInterface } from '../interfaces/product.interface';

export class Clothes implements ProductInterface {
  constructor(
    public id: number,
    public name: string,
    public size: string,
    public color: string,
  ) {}

  public getInfo(id: number) {
    const data = this.getData().find((d) => (d.id = id));
    if (!data) {
      throw new Error('Product Not Found');
    }
    return `Clothes - ID: ${data.id}, Name: ${data.name}, Size: ${data.size}, Color: ${data.color}`;
  }

  public getData() {
    return [
      { id: 1, name: 'sơ mi', size: 'XL', color: 'đen' },
      { id: 2, name: 'sơ polo', size: 'L', color: 'đen' },
    ];
  }
}
