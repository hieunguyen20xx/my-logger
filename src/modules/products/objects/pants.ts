import { ProductInterface } from '../interfaces/product.interface';

export class Pants implements ProductInterface {
  constructor(
    public id: string,
    public name: string,
    public size: string,
    public color: string,
  ) {}

  public getInfo(id: number) {
    const data = this.getData().find((d) => (d.id = id));
    if (!data) {
      throw new Error('Product Not Found');
    }
    return `Pants - ID: ${data.id}, Name: ${data.name}, Size: ${data.size}, Color: ${data.color}`;
  }

  public getData() {
    return [
      { id: 1, name: 'âu', size: '31', color: 'đen' },
      { id: 2, name: 'kaki', size: '32', color: 'trắng' },
    ];
  }
}
