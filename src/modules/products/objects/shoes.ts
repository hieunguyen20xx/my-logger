import { ProductInterface } from '../interfaces/product.interface';

export class Shoes implements ProductInterface {
  constructor(
    public id: string,
    public name: string,
    public size: string,
    public color: string,
  ) {}

  public getInfo(id: number) {
    const data = this.getData().find((d) => (d.id = id));
    if (!data) {
      throw new Error('Product Not Found');
    }
    return `Shoes - ID: ${data.id}, Name: ${data.name}, Size: ${data.size}, Color: ${data.color}`;
  }

  public getData() {
    return [
      { id: 1, name: 'stan smith', size: '34', color: 'đen trắng' },
      { id: 2, name: 'bitis hunter', size: '30', color: 'đen' },
    ];
  }
}
