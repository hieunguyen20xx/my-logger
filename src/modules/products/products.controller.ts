import {
  Controller,
  Get,
  Param,
  Query,
  Res,
  StreamableFile,
} from '@nestjs/common';
import { ProductType } from './constants/constants';
import { ProductsService } from './products.service';
import { createReadStream } from 'fs';
import { join } from 'path';
import type { Response } from 'express';

@Controller('products')
export class ProductsController {
  constructor(private readonly productService: ProductsService) {}

  @Get(':productId')
  getInfoProductByType(
    @Param('productId') productId: number,
    @Query('type') type: ProductType,
  ) {
    return this.productService.getInfoProductByType(productId, type);
  }

  @Get('file/stream')
  getFile(@Res({ passthrough: true }) res: Response): StreamableFile {
    const file = createReadStream(join(process.cwd(), 'package.json'));
    res.set({
      'Content-Type': 'application/json',
      'Content-Disposition': 'attachment; filename="package.json',
    });
    return new StreamableFile(file);
  }
}
