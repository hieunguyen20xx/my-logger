import { Module } from '@nestjs/common';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import { ProductFactory } from './factories/product.factory';
import { Clothes } from './objects/clothes';
import { Pants } from './objects/pants';
import { Shoes } from './objects/shoes';

@Module({
  controllers: [ProductsController],
  providers: [ProductsService, ProductFactory, Clothes, Pants, Shoes],
  exports: [ProductsService, ProductFactory, Clothes, Pants, Shoes],
})
export class ProductsModule {}
