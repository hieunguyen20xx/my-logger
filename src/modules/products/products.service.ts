import { Injectable } from '@nestjs/common';
import { ProductType } from './constants/constants';
import { ProductFactory } from './factories/product.factory';

@Injectable()
export class ProductsService {
  constructor(private readonly productFactory: ProductFactory) {}

  async getInfoProductByType(productId: number, type: ProductType) {
    const productObject = await this.productFactory.createObject(type);

    return productObject.getInfo(productId);
  }
}
