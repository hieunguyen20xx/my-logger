import { IsString, MaxLength } from 'class-validator';

export class CreateTweetDto {
  @MaxLength(1000)
  @IsString()
  content: string;
}
