import { Body, Controller, Get, Post, Request } from '@nestjs/common';
import { TweetService } from './tweets.service';
import { CreateTweetDto } from './dtos/create-new-tweet';
import { Public } from '../auth/auth.decorator';
import { CurrentUser } from '../auth/user.decorator';

@Controller('tweet')
export class TweetController {
  constructor(private readonly tweetService: TweetService) {}

  @Post()
  async createTweet(@Body() data: CreateTweetDto, @CurrentUser() user) {
    const { userId } = user;
    return await this.tweetService.createTweet({ ...data, userId });
  }
}
