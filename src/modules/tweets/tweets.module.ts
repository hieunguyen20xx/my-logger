import { Module } from '@nestjs/common';
import { PrismaService } from 'src/database/prisma.service';
import { TweetService } from './tweets.service';
import { TweetController } from './tweets.controller';

@Module({
  controllers: [TweetController],
  providers: [TweetService, PrismaService],
  exports: [TweetService],
})
export class TweetsModule {}
