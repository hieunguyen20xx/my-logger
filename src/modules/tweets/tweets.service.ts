import { Inject, Injectable } from '@nestjs/common';
import { Prisma, Tweet, User } from '@prisma/client';
import { PrismaService } from 'src/database/prisma.service';

@Injectable()
export class TweetService {
  constructor(private prisma: PrismaService) {}

  async createTweet(data: {
    content: Tweet[`content`];
    userId: User[`id`];
  }): Promise<Tweet> {
    return await this.prisma.tweet.create({ data });
  }
}
